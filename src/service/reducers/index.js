import { changeTheNum, todoReducer } from "./reducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  changeTheNum,
  todoReducer,
});

export default rootReducer;
