import {
  INCREAMENT,
  DECREAMENT,
  ADD_TODO,
  DELETE_TODO,
  REMOVE_TODO,
  UPDATE_TODO,
} from "../Const";

export const incNumber = (num) => {
  return {
    type: INCREAMENT,
    payload: num,
  };
};

export const drcNumber = () => {
  return {
    type: DECREAMENT,
  };
};

export const addTodo = (formData) => {
  return {
    type: ADD_TODO,
    payload: {
      id: formData.id,
      name: formData.name,
      subtext: formData.subtext,
      date: JSON.stringify(formData.date),
      subject: formData.subject,
      checked: formData.checked,
      picked: formData.picked,
      movie: formData.movie,
    },
  };
};

export const deleteTodo = (id) => {
  return {
    type: DELETE_TODO,
    payload: {
      id: id,
    },
  };
};

export const UpdateTodo = (udateData) => {
  return {
    type: UPDATE_TODO,
    payload: {
      uid: udateData.id,
      uname: udateData.name,
      usubtext: udateData.subtext,
      udate: JSON.stringify(udateData.date),
      usubject: udateData.subject,
      checked: udateData.checked,
      upicked: udateData.picked,
      umovie: udateData.movie,
    },
  };
};

export const removeTodo = () => {
  return {
    type: REMOVE_TODO,
  };
};
