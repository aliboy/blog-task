import { LOADER } from "./actionType";

export default (data) => (dispatch) => {
  dispatch({
    type: LOADER,
    data,
  });
};
