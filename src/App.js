import { styled } from "@mui/material/styles";
import { Box } from "@mui/system";
import { BrowserRouter } from "react-router-dom";
import Sidebar from "./component/header/Sidebar";
import Routess from "./component/Routes";

import "./App.css";

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

function App() {
  return (
    <Box width="100%">
      <BrowserRouter>
        <Box sx={{ display: "flex" }}>
          <Sidebar />
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <DrawerHeader />
            <Routess />
          </Box>
        </Box>
      </BrowserRouter>
    </Box>
  );
}

export default App;
