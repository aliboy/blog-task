const LocalStorageService = (function () {
  var _service
  function _getService() {
    if (!_service) {
      _service = this
      return _service
    }
    return _service
  }
  function _setToken(tokenObj) {
    localStorage.setItem('auth_data', JSON.stringify(tokenObj))
  }
  function _getAccessToken() {
    return JSON.parse(localStorage.getItem('auth_data'))
  }
  function _clearToken() {
    localStorage.removeItem('auth_data')
  }
  function _setAdminToken(tokenObj) {
    localStorage.setItem('admin_auth_data', JSON.stringify(tokenObj))
  }
  function _getAdminAccessToken() {
    return JSON.parse(localStorage.getItem('admin_auth_data'))
  }
  function _clearAdminToken() {
    localStorage.removeItem('admin_auth_data')
  }
  return {
    getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    clearToken: _clearToken,
    setAdminToken: _setAdminToken,
    getAdminAccessToken: _getAdminAccessToken,
    clearAdminToken: _clearAdminToken,
  }
})()
export default LocalStorageService
