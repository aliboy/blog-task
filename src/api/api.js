export const baseUrl = "https://gorest.co.in";
export const API = {
  auth: {
    login: `/account/api/auth/login`,
    register: `/account/api/auth/register`,
    refresh: `/account/api/auth/refresh`,
    forgotPassword: `/account/api/auth/passwordreset`,
    verification: `/account/api/auth/payload-verification`,
    logout: "/account/-/account/logout",
    verify: "/account/-/account/verify-password",
  },
  user: {
    userList: `${baseUrl}/public/v2/users`,
    searchUsers: `/account/-/users`,
  },
};
