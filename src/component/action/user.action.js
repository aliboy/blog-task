import { API } from "../../api/api";
import { fetch } from "../../api/httpClient";

export const fetchUsersApi =
  (successCallback, failureCallback) => async (dispatch) => {
    try {
      const apiPath = `${API.user.userList}`;
      const response = await fetch(apiPath, {});
      if (typeof successCallback === "function") {
        successCallback(response);
      }
    } catch (err) {
      if (typeof failureCallback === "function") {
        failureCallback(err);
      }
    } finally {
    }
  };
