import React, { lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import Loader from "./loader/Loader";
const Home = lazy(() => import("./home/Home"));
const Login = lazy(() => import("./login/Login"));
const SignUpWithState = lazy(() => import("./signup/SignUpWithState"));
const ToDoMap = lazy(() => import("./todo/ToDoMap"));

function Routess() {
  return (
    <Suspense fallback={<Loader />}>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUpWithState />} />
        <Route path="/todo" element={<ToDoMap />} />
      </Routes>
    </Suspense>
  );
}

export default Routess;
