import { Button, Paper, TextField, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { fetchUsersApi } from "../action/user.action";

function Login() {
  const loginId = useParams();
  const getLocalData = JSON.parse(localStorage.getItem("signupdata"))
    ? JSON.parse(localStorage.getItem("signupdata"))
    : [];
  const userUrl = getLocalData.find(
    (element) => element.email === loginId.email
  );
  console.log(userUrl);

  function useQuery() {
    // Use the URLSearchParams API to extract the query parameters
    // useLocation().search will have the query parameters eg: ?foo=bar&a=b
    return new URLSearchParams(useLocation().search);
  }

  const query = useQuery();
  const email = query.get("email");
  const pass = query.get("password");
  console.log("email= " + email);
  console.log("pass=" + pass);
  const userUrlQuery = getLocalData.find((element) => element.email === email);
  console.log(userUrlQuery);
  if (email) {
    if (userUrlQuery) {
      if (userUrlQuery.email === email && userUrlQuery.password === pass) {
        localStorage.setItem("login", email);
        window.location.reload();
        window.location = "/home";
      } else alert("Wrong Credentials");
    } else alert("User not registered");
  }

  if (loginId.email) {
    if (userUrl) {
      if (
        userUrl.email === loginId.email &&
        userUrl.password === loginId.password
      ) {
        console.log(loginId);
        localStorage.setItem("login", loginId.email);
        window.location.reload();
        // document.location.reload(true);
        // window.location = `/${loginId.email}/${loginId.password}`;
        window.location = "/home";
      } else alert("Wrong Credentials");
    } else alert("User not registered");
  }

  const initialValues = {
    email: "",
    password: "",
  };

  const validationSchema = Yup.object({
    email: Yup.string()
      .email("Invailid email format")
      .required("Required email field"),
    password: Yup.string().required("Required password field"),
  });

  const onSubmit = (values, onSubmitProps) => {
    const user = getLocalData.find((element) => element.email === values.email);
    if (user) {
      if (values.email === user.email && values.password === user.password) {
        localStorage.setItem("login", JSON.stringify(user.email));
        window.location = "/todo";
      }
    } else alert("Please enter valid credentails");
    // setTimeout(() => {
    //   onSubmitProps.setSubmitting(false);
    // }, 2000);
  };

  React.useState(() => {
    console.log("object");
  });

  return (
    <Box
      sx={{
        width: 500,
        maxWidth: "100%",
      }}
      mt={5}
      mx="auto"
    >
      <Paper variant="outlined">
        <Box p={3}>
          <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
          >
            {({ isSubmitting }) => {
              return (
                <Form>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="email"
                      size="small"
                      label="Email"
                      id="email"
                      name="email"
                    />
                    <ErrorMessage name="email">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="password"
                      size="small"
                      label="Password"
                      id="password"
                      name="password"
                    />
                    <ErrorMessage name="password">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    // disabled={isSubmitting}
                  >
                    Submit
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Paper>
    </Box>
  );
}

export default Login;
