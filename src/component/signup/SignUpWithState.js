import { Button, Paper, TextField, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import React from "react";

function SignUpWithState() {
  const data = JSON.parse(localStorage.getItem("signupdata"));
  const getLocalData = data ? data : [];
  let [userData, setUserData] = React.useState(getLocalData);

  const initialValues = {
    name: "",
    email: "",
    password: "",
  };

  const validationSchema = Yup.object({
    name: Yup.string().required("Required Name Field"),
    email: Yup.string()
      .email("Invailid email format")
      .required("Required Email Field"),
    password: Yup.string()
      .min(6, "Password must be 6 characters at minimum")
      .required("Password is required"),
  });

  const onSubmit = (values, { resetForm }) => {
    const existUser = getLocalData.find(
      (element) => element.email === values.email
    );
    if (existUser && values.email === existUser.email) {
      alert("User Already registered");
    } else {
      setUserData([...getLocalData, values]);
      resetForm();
      window.location = "/login";
    }
  };
  localStorage.setItem("signupdata", JSON.stringify(userData));

  return (
    <Box
      sx={{
        width: 500,
        maxWidth: "100%",
      }}
      mt={5}
      mx="auto"
    >
      <Paper variant="outlined">
        <Box p={3}>
          <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
          >
            {() => {
              return (
                <Form>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="text"
                      size="small"
                      label="Name"
                      id="name"
                      name="name"
                    />
                    <ErrorMessage name="name">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="email"
                      size="small"
                      label="Email"
                      id="email"
                      name="email"
                    />
                    <ErrorMessage name="email">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="password"
                      size="small"
                      label="Password"
                      id="password"
                      name="password"
                    />
                    <ErrorMessage name="password">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Button type="submit" color="primary" variant="contained">
                    Submit
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </Box>
        {/* {userData.map((val) => (
          <li key={val.email}>{val.name}</li>
        ))} */}
      </Paper>
    </Box>
  );
}

export default SignUpWithState;
