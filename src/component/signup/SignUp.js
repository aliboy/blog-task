import { Button, Paper, TextField, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { GithubPicker } from "react-color";
import React from "react";

function SignUp() {
  const [showColorPicker, setShowColorPicker] = React.useState(false);
  const handleColorpicker = () => {
    setShowColorPicker(!showColorPicker);
  };
  const handleBackdrop = () => {
    setShowColorPicker(!showColorPicker);
  };

  let getLocalData = JSON.parse(localStorage.getItem("signupdata"));
  const dataArr = getLocalData ? getLocalData : [];

  const initialValues = {
    name: "",
    email: "",
    password: "",
  };

  const validationSchema = Yup.object({
    name: Yup.string().required("Required"),
    email: Yup.string().email("Invailid email format").required("Required"),
    password: Yup.string().required("Required"),
  });

  const onSubmit = (values, { resetForm }) => {
    dataArr.push(values);
    localStorage.setItem("signupdata", JSON.stringify([...dataArr]));
    resetForm();
    window.location = "/login";
  };

  return (
    <Box
      sx={{
        width: 500,
        maxWidth: "100%",
      }}
      mt={5}
    >
      <Paper variant="outlined">
        <Box p={3}>
          <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
          >
            {() => {
              return (
                <Form>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="text"
                      size="small"
                      label="Name"
                      id="name"
                      name="name"
                    />
                    <ErrorMessage name="name">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="email"
                      size="small"
                      label="Email"
                      id="email"
                      name="email"
                    />
                    <ErrorMessage name="email">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="password"
                      size="small"
                      label="Password"
                      id="password"
                      name="password"
                    />
                    <ErrorMessage name="password">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Button type="submit" color="primary" variant="contained">
                    Submit
                  </Button>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={handleColorpicker}
                  >
                    show picker
                  </Button>

                  {showColorPicker ? <GithubPicker /> : null}
                  {showColorPicker && (
                    <Box
                      className="backgroundOverlay"
                      onClick={handleBackdrop}
                    ></Box>
                  )}
                </Form>
              );
            }}
          </Formik>
        </Box>
        {getLocalData?.map((val) => (
          <li key={val.email}>{val.name}</li>
        ))}
      </Paper>
    </Box>
  );
}

export default SignUp;
