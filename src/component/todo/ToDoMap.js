import React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import FormControl from "@mui/material/FormControl";
import FolderIcon from "@mui/icons-material/Folder";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  Autocomplete,
  Avatar,
  Button,
  Checkbox,
  Container,
  Grid,
  InputLabel,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";

import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import {
  addTodo,
  deleteTodo,
  removeTodo,
  UpdateTodo,
} from "../../service/actions";
import { useTranslation } from "react-i18next";
import { v4 as uuidv4 } from "uuid";
import DatePicker from "../common/DatePicker";
import FormikAutocomplete from "../common/FormikAutoComplete";

function ToDoMap({
  listMap,
  removeTodoMap,
  addTodoMap,
  UpdateTodoMap,
  deleteTodoMap,
}) {
  const { t } = useTranslation();
  const myuuid = uuidv4();

  const initData = {
    id: myuuid,
    name: "",
    subtext: "",
    date: "",
    subject: "All",
    checked: [],
    picked: "",
    movie: "",
    skills: [
      {
        label: "Swift",
        value: "Swift",
      },
      {
        label: "Webpack",
        value: "Webpack",
      },
    ],
  };
  const [inputData, setInputData] = React.useState(initData);
  const [updateBtn, setUpdateBtn] = React.useState(true);
  localStorage.setItem("todo", JSON.stringify(listMap));

  const validationSchema = Yup.object({
    id: Yup.string().required("Required id field"),
    name: Yup.string().required("Required name Field"),
    subtext: Yup.string().required("Required designation field"),
    date: Yup.date().required("Required date field"),
  });

  const addDataToList = (values, { resetForm }) => {
    let existUser = listMap.filter((user) => user.id === values.id);
    if (existUser.length) {
      alert("existing user");
    } else {
      if (values.id) {
        addTodoMap(values);
        resetForm();
        setInputData({ ...inputData, id: myuuid });
      } else alert("Please enter user id");
    }
    console.log(values);
  };

  const editToDOList = (id) => {
    const editdo = listMap.find((e) => e.id === id);
    setInputData({
      ...inputData,
      id: editdo.id,
      name: editdo.name,
      subtext: editdo.subtext,
      date: JSON.parse(editdo.date),
      subject: editdo.subject,
    });
    setUpdateBtn(false);
  };

  const updateToDoList = (userData) => {
    UpdateTodoMap(userData);
    setInputData(initData);
  };

  const top100Films = [
    { label: "The Shawshank Redemption", year: 1994 },
    { label: "The Godfather", year: 1972 },
    { label: "The Godfather: Part II", year: 1974 },
    { label: "The Dark Knight", year: 2008 },
    { label: "12 Angry Men", year: 1957 },
    { label: "Schindler's List", year: 1993 },
    { label: "Pulp Fiction", year: 1994 },
  ];

  const skills = [
    {
      label: "PostgreSQL",
      value: "PostgreSQL",
    },
    {
      label: "Pythonaa",
      value: "Pythona",
    },
    {
      label: "React",
      value: "React",
    },
    {
      label: "Redis",
      value: "Redis",
    },
    {
      label: "Swift",
      value: "Swift",
    },
    {
      label: "Webpack",
      value: "Webpack",
    },
  ];

  return (
    <Box mt={4}>
      <Container maxWidth="sm">
        <Formik
          initialValues={inputData}
          onSubmit={addDataToList}
          validationSchema={validationSchema}
          enableReinitialize={true}
        >
          {({ values, setFieldValue }) => {
            return (
              <Form>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <FormControl sx={{ width: "100%" }}>
                      <Field
                        as={TextField}
                        id="id"
                        label="ID"
                        name="id"
                        variant="outlined"
                        value={values.id}
                        disabled
                      />
                    </FormControl>
                    <ErrorMessage name="id">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl sx={{ width: "100%" }}>
                      <Field
                        as={TextField}
                        id="name"
                        label="Name"
                        name="name"
                        variant="outlined"
                        value={values.name}
                      />
                    </FormControl>
                    <ErrorMessage name="name">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl sx={{ width: "100%" }}>
                      <Field
                        as={TextField}
                        id="subtext"
                        label="Designation"
                        name="subtext"
                        variant="outlined"
                        value={values.subtext}
                      />
                    </FormControl>
                    <ErrorMessage name="subtext">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl sx={{ width: "100%" }}>
                      <Field
                        component={DatePicker}
                        id="date"
                        label="DOB"
                        name="date"
                        variant="outlined"
                        value={values.date}
                        onChange={(value) => setFieldValue("date", value)}
                      />
                    </FormControl>
                    <ErrorMessage name="date">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl sx={{ width: "100%" }}>
                      <InputLabel id="subject">Subject</InputLabel>
                      <Field
                        as={Select}
                        id="subject"
                        label="Subject"
                        name="subject"
                        variant="outlined"
                        value={values.subject}
                        displayEmpty
                      >
                        <MenuItem value={"All"}>All</MenuItem>
                        <MenuItem value={"Create"}>Create</MenuItem>
                        <MenuItem value={"Read"}>Read</MenuItem>
                        <MenuItem value={"Update"}>Update</MenuItem>
                        <MenuItem value={"Delete"}>Delete</MenuItem>
                      </Field>
                    </FormControl>
                    <ErrorMessage name="subject">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Grid>
                  <Grid item xs={12}>
                    {/* <Field
                      id="checkbox"
                      label="checkbox"
                      name="checkbox"
                      variant="outlined"
                      value={values.checkbox}
                    >
                      <FormGroup>
                        <FormControlLabel
                          control={<Checkbox defaultChecked />}
                          label="One"
                        />
                        <FormControlLabel control={<Checkbox />} label="Two" />
                      </FormGroup>
                    </Field> */}
                    {/* <FormikCheckBox
                      checked={values.checked[0]}
                      name="checked"
                      label={"One"}
                    />
                    <FormikCheckBox
                      checked={values.checked[1]}
                      name="checked"
                      label={"Two"}
                    />
                    <FormikCheckBox
                      checked={values.checked[2]}
                      name="checked"
                      label={"Three"}
                    /> */}
                    <label htmlFor="checkbox1">One</label>
                    <Field
                      as={Checkbox}
                      id="checkbox1"
                      type="checkbox"
                      name="checked"
                      value="One"
                    />
                    <label htmlFor="checkbox2">Two</label>
                    <Field
                      as={Checkbox}
                      id="checkbox2"
                      type="checkbox"
                      name="checked"
                      value="Two"
                    />
                    <label htmlFor="checkbox3">Three</label>
                    <Field
                      as={Checkbox}
                      id="checkbox3"
                      type="checkbox"
                      name="checked"
                      value="Three"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <div role="group" aria-labelledby="my-radio-group">
                      <label>
                        <Field type="radio" name="picked" value="Radio One" />
                        One
                      </label>
                      <label>
                        <Field type="radio" name="picked" value="Radio Two" />
                        Two
                      </label>
                    </div>
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      component={Autocomplete}
                      name="movie"
                      id="movie"
                      options={top100Films}
                      isOptionEqualToValue={(item, current) => {
                        return item.label === current.label;
                      }}
                      getOptionLabel={(option) => option.label}
                      renderOption={(props, option) => (
                        <Box component="li" {...props}>
                          {option.label} ({option.year})
                        </Box>
                      )}
                      onChange={(_, value) => {
                        setFieldValue("movie", value?.label);
                      }}
                      renderInput={(params) => (
                        <TextField {...params} label="Movie" />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      name="skills"
                      component={FormikAutocomplete}
                      label="Skills"
                      options={skills}
                      multiple
                    />
                  </Grid>
                  <Grid container spacing={2} item xs={12}>
                    <Grid item>
                      <Button
                        type="submit"
                        variant="contained"
                        disableElevation
                      >
                        Add
                      </Button>
                    </Grid>
                    <Grid item>
                      <Button
                        disabled={updateBtn}
                        variant="contained"
                        disableElevation
                        onClick={() => updateToDoList(values)}
                      >
                        Update
                      </Button>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      sx={{ mt: 4, mb: 2 }}
                      variant="h6"
                      component="div"
                    >
                      {t("TodoDesc")}
                    </Typography>
                    <List dense={false}>
                      {listMap.map((data) => (
                        <ListItem
                          key={data.id}
                          secondaryAction={
                            <>
                              <IconButton
                                edge="end"
                                onClick={() => editToDOList(data.id)}
                              >
                                <EditIcon />
                              </IconButton>
                              <IconButton
                                edge="end"
                                onClick={() => deleteTodoMap(data.id)}
                              >
                                <DeleteIcon />
                              </IconButton>
                            </>
                          }
                        >
                          <ListItemAvatar>
                            <Avatar>
                              <FolderIcon />
                            </Avatar>
                          </ListItemAvatar>
                          <ListItemText
                            primary={data.name}
                            secondary={
                              <>
                                <Box component="span" display="block">
                                  {data.subtext}
                                </Box>
                                <Box component="span" display="block">
                                  {data.date}
                                </Box>
                                <Box component="span" display="block">
                                  {data.subject}
                                </Box>
                                <Box component="span" display="block">
                                  {data.checked}
                                </Box>
                                <Box component="span" display="block">
                                  {data.picked}
                                </Box>
                                <Box component="span" display="block">
                                  {data.movie}
                                </Box>
                                <Box component="span" display="block">
                                  {/* {data.skills.value} */}
                                </Box>
                              </>
                            }
                          />
                        </ListItem>
                      ))}
                    </List>
                    <Button
                      color="error"
                      variant="contained"
                      onClick={() => removeTodoMap()}
                    >
                      Remove All
                    </Button>
                  </Grid>
                </Grid>
              </Form>
            );
          }}
        </Formik>
      </Container>
    </Box>
  );
}

const mapStateToProps = (state) => {
  return {
    listMap: state.todoReducer.toDolist,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addTodoMap: (data) => dispatch(addTodo(data)),
    UpdateTodoMap: (data) => dispatch(UpdateTodo(data)),
    deleteTodoMap: (id) => dispatch(deleteTodo(id)),
    removeTodoMap: () => dispatch(removeTodo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoMap);
