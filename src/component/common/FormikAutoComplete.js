import { TextField } from "@mui/material";
import { Autocomplete } from "@mui/material";
import React from "react";

const FormikAutocomplete = ({ ...props }) => {
  const { field, form } = props;
  // const { error, helperText, ...field } = fieldToTextField(props)
  const { name } = field;

  return (
    <Autocomplete
      {...props}
      {...field}
      onChange={(_, value) => form.setFieldValue(name, value)}
      isOptionEqualToValue={(item, current) => item.value === current.value}
      renderInput={(props) => <TextField {...props} label="Skills" />}
    />
  );
};

export default FormikAutocomplete;
