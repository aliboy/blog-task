import React from "react";
import { TextField } from "@mui/material";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DesktopDatePicker from "@mui/lab/DesktopDatePicker";

export default function DatePicker({ field, form }) {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DesktopDatePicker
        name={field.name}
        value={field.value}
        label="Date of Birth"
        inputFormat="MM/dd/yyyy"
        renderInput={(params) => <TextField {...params} />}
        onChange={(date) => form.setFieldValue(field.name, date, false)}
      />
    </LocalizationProvider>
  );
}
